import type { Ref } from "vue";
import { ref, watch } from "vue";

export default function useElementSize(el: Ref<HTMLDivElement | undefined>) {
  const width = ref(0);
  const height = ref(0);
  console.log(el);
  let observer: undefined | ResizeObserver = undefined;

  const disconnect = () => {
    if (observer) {
      observer.disconnect();
      observer = undefined;
    }
  };

  const connect = (element: HTMLElement) => {
    disconnect();
    observer = new ResizeObserver((entries) => {
      const [
        {
          contentRect: { width: elWidth, height: elHeight },
        },
      ] = entries;
      width.value = elWidth;
      height.value = elHeight;
    });

    observer.observe(element);
  };

  watch(el!, (element) => {
    if (element) connect(element);
    else disconnect();
  });

  return {
    width,
    height,
  };
}
